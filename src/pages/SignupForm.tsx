import styled from "styled-components";
import { useForm } from "react-hook-form";
import SubmitButton from "../components/SubmitButton";
import ErrorMessage from "../components/ErrorMessage";
import LabelForm from "../components/LabelForm";
import Box from "../components/Box";

const StyledInput = styled.input`
  border: none;
  border-radius: 5px;
  width: 80%;
  height: 50px;
  background-color: #ffffff;
  margin: 0 auto;

  &:hover {
    background-color: #bebebe;
  }

  &:focus {
    outline: none !important;
    border-color: #719ece;
    box-shadow: 0 0 10px #719ece;
  }
`;

const StyledForm = styled.form`
  background-color: #1b0017;
  width: 40%;
  margin: 0 auto;
  padding: 20px;
  border-radius: 10px;
`;

const SignupForm = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      firstname: "",
      lastname: "",
      birthdate: "",
      email: "",
    },
  });

  const firstname = watch("firstname");

  console.log(firstname);

  return (
    <StyledForm
      className="form"
      onSubmit={handleSubmit((data) => {
        console.log(data);
      })}
    >
      <Box>
        <LabelForm htmlFor="firstname">First Name</LabelForm>
        <StyledInput
          {...register("firstname", {
            required: "First Name is required.",
            pattern: /^[A-Za-z]+$/i,
          })}
          id="firstname"
          type="text"
          placeholder="John"
        />
        <ErrorMessage>{errors.firstname?.message}</ErrorMessage>
      </Box>

      <Box>
        <LabelForm htmlFor="lastname">Last Name</LabelForm>
        <StyledInput
          {...register("lastname", {
            required: "Last name is required.",
            pattern: /^[A-Za-z]+$/i,
            minLength: {
              value: 4,
              message: "Min length is 4.",
            },
          })}
          id="lastname"
          type="text"
          placeholder="Doe"
        />
        <ErrorMessage>{errors.lastname?.message}</ErrorMessage>
      </Box>

      <Box>
        <LabelForm htmlFor="birthdate">Birth date</LabelForm>
        <StyledInput
          {...register("birthdate", {
            required: "Please enter your birthdate",
          })}
          id="birthdate"
          type="date"
        />
        <ErrorMessage>{errors.birthdate?.message}</ErrorMessage>
      </Box>

      <Box>
        <LabelForm htmlFor="email">Email address</LabelForm>
        <StyledInput {...register("email")} id="email" type="email" />
      </Box>

      <Box>
        <LabelForm htmlFor="password">Password</LabelForm>
        <StyledInput type="password" />
      </Box>
      <Box>
        <SubmitButton>Sign up</SubmitButton>
      </Box>
    </StyledForm>
  );
};

export default SignupForm;
