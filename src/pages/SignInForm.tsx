import styled from "styled-components";
import SubmitButton from "../components/SubmitButton";
import FormSignIn from "../components/FormSignIn";
import LabelForm from "../components/LabelForm";

const StyledInput = styled.input`
  border: none;
  border-radius: 5px;
  width: 80%;
  height: 50px;
  background-color: #ffffff;
  margin: 0 auto;

  &:hover {
    background-color: #f0d0ff; // <Thing> when hovered
  }
`;

export const Box = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  text-align: center;
`;

const SignInForm = () => {
  return (
    <FormSignIn>
      <Box>
        <LabelForm htmlFor="firstname">First Name</LabelForm>
        <StyledInput id="firstname" type="text" />
      </Box>

      <Box>
        <LabelForm htmlFor="lastname">Last Name</LabelForm>
        <StyledInput id="lastname" type="text" />
      </Box>

      <Box>
        <LabelForm htmlFor="birthdate">Birth date</LabelForm>
        <StyledInput id="birthdate" type="date" />
      </Box>

      <Box>
        <LabelForm htmlFor="email">Email address</LabelForm>
        <StyledInput type="email" />
      </Box>

      <Box>
        <LabelForm htmlFor="password">Password</LabelForm>
        <StyledInput type="password" />
      </Box>
      <Box>
        <SubmitButton>Sign In</SubmitButton>
      </Box>
    </FormSignIn>
  );
};

export default SignInForm;
