import React from "react";
import styled from "styled-components";

export const StyledBox = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  text-align: center;
`;

function Box({ children }) {
  return <StyledBox>{children}</StyledBox>;
}

export default Box;
