import React from "react";
import styled from "styled-components";

const StyledForm = styled.form`
  background-color: #1b0017;
  width: 40%;
  margin: 0 auto;
  padding: 20px;
  border-radius: 10px;
`;

function FormSignIn({ children }) {
  return <StyledForm>{children}</StyledForm>;
}

export default FormSignIn;
