import React from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  border: none;
  border-radius: 5px;
  width: 80%;
  height: 50px;
  background-color: #ffffff;
  margin: 0 auto;

  &:hover {
    background-color: #420061; // <Thing> when hovered
  }
`;

function InputForm({ name, id, type, innerRef, placeholder }) {
  return (
    <StyledInput
      innerRef={forwardRef}
      id={id}
      name={name}
      type={type}
      placeholder={placeholder}
    />
  );
}

export default InputForm;
