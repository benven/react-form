import React from "react";
import styled from "styled-components";

const StyledErrorMessage = styled.p`
  color: #ff7b00;
  font-size: 1rem;
`;

function ErrorMessage({ children }) {
  return <StyledErrorMessage>{children}</StyledErrorMessage>;
}

export default ErrorMessage;
