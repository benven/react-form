import React from "react";
import styled from "styled-components";

const StyledLabelForm = styled.label`
  font-size: 1.2rem;
  font-weight: bold;
  margin-bottom: 10px;
  color: white;
`;

function LabelForm({ htmlFor, children }) {
  return <StyledLabelForm htmlFor={htmlFor}>{children}</StyledLabelForm>;
}

export default LabelForm;
