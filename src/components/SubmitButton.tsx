import React from "react";
import styled from "styled-components";

const StyledSubmitButton = styled.button`
  background-color: #b92fb9;
  padding: 10px 40px;
  width: 200px;
  border: none;
  color: white;
  border-radius: 5px;
  margin: 20px auto;

  &:hover {
    background-color: #681568;
    cursor: pointer;
  }
`;

function SubmitButton({ children }) {
  return <StyledSubmitButton type="submit">{children}</StyledSubmitButton>;
}

export default SubmitButton;
